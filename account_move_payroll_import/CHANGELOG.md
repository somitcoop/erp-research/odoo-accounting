# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.1.0.1] - 2024-09-23
### Fixed
- [#4](https://gitlab.com/somitcoop/erp-research/odoo-accounting/-/merge_requests/4) Fix missing default mapping for new import setup templates

## [12.0.1.0.0] - 2024-07-10
### Added
- [#2](https://gitlab.com/somitcoop/erp-research/odoo-accounting/-/merge_requests/2) Add account_move_payroll_import
