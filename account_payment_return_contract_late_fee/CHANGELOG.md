# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [12.0.1.0.0] - 2024-11-20
### Added
- [#5](https://gitlab.com/somitcoop/erp-research/odoo-accounting/-/merge_requests/5) Add the new module account_payment_return_contract_late_fee
