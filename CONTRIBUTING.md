
# odoo-documentation

Contribute to this project to build odoo addons that integrates ID documentation and its validation to CRM.

###  New module creation guide

1. Create a branch from main and name it as "odoo12" before the module name, separated with hypens. Example: "odoo12-crm-documentation-api"

2. From the local container, once it is mounted in "/opt/odoo_modules" (see README), execute the following command to create the basic structure of a new module inside our application:

```
./odoo-bin scaffold <<my_module>> /opt/odoo_modules/<<odoo-repo>>/
```

3. Update its **MANIFEST** by adding dependencies and starting with version 12.0.0.1.0.

4. Build the module and its architecture following the [OCA guidelines]([#oca-module-guidelines](https://www.odoo.com/documentation/12.0/developer/reference/guidelines.html))

5.  Run pylint and correct all of its errors:

```
pylint --rcfile=.pylintrc *
```

7. Export and fill the translation files (.po) from the new module through the UI, for catalan and spanish languages.

8. Create a **CHANGELOG** starting with version 12.0.0.1.0.

9.  Fill a **README** following the structure from the OCA ([example]/(https://github.com/OCA/helpdesk/blob/15.0/helpdesk_mgmt/README.rst)), making sure the new module's functionally is well described.

10. Add new module in setup, using setuptools-odoo as described [here](https://handbook.coopdevs.org/Odoo/Sysadmin/Packaging-Odoo-Addons). Fix dependencies versions in the generated setup.py.

11. Create a commit with the addon and name it "[ADD] <<new-module>> module".

12. Add new module to addons list in the repository README.

13. (SOON) Migrate new module to ODOO version 16.
